package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private Integer rows;
    private Integer columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows; 
        this.columns = columns;
        if ((rows <= 0) || (columns <= 0)){
            throw new IllegalArgumentException("Rows and columns must be positive");
        }
        if (!initialState.equals(CellState.DEAD) && !initialState.equals(CellState.ALIVE)) {
            throw new IllegalArgumentException("Must be dead or alive");
        }
        this.grid = new CellState[rows][columns];
        for (int r = 0; r<rows; r++) { 
            for (int c = 0; c<columns; c++) { 
                this.grid[r][c] = initialState;
            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid copy = new CellGrid(rows, columns, CellState.ALIVE); 
        for (int r = 0; r<rows; r++) { 
            for (int c = 0; c<columns; c++) { 
                CellState newstate = this.get(r,c);
                copy.set(r, c, newstate);
            }
        }
        return copy;
    }
    
}
