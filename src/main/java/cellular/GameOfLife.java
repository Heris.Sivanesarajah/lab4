package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	Integer rows;
	Integer columns;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */

	public GameOfLife(int rows, int columns) {
		this.rows = rows; 
		this.columns = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return this.rows;
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return this.columns;
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int r = 0; r<rows; r++) {
			for (int c = 0; c<columns; c++) {
				CellState newState = getNextCell(r, c);
				nextGeneration.set(r, c, newState);
			}
		}
		currentGeneration = nextGeneration.copy();
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState nextState = CellState.DEAD;
		CellState currentState = getCellState(row, col);
		if (aliveNeighbors == 3) {
			nextState = CellState.ALIVE;
		}
		else if (currentState == CellState.ALIVE && aliveNeighbors<=3 && aliveNeighbors>1) {
			nextState = CellState.ALIVE;
		}
		return nextState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		Integer x = row;
		Integer y = col;
		Integer statecount = 0; 
		if (x > 0 && x < numberOfRows() - 1 && y > 0 && y < numberOfColumns() - 1) {
			for (int xi = x-1; xi<x+2;xi++) {
				for (int yi = y-1; yi<y+2;yi++) {
					if (xi == x && yi == y) {
						continue;
					}
					else if (state == currentGeneration.get(xi, yi)) {
						statecount++;
					}
				}
			}
		}
		else {
			if (x == 0) {
				if (y == 0) {
					for (int xi = x; xi<x+2;xi++) {
						for (int yi = y; yi<y+2;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
				else if (y == numberOfColumns()-1) {
					for (int xi = x; xi<x+2;xi++) {
						for (int yi = y-1; yi<y+1;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
			}
			else { 
				if (y == 0) {
					for (int xi = x-1; xi<x+1;xi++) {
						for (int yi = y; yi<y+2;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
				else if (y == numberOfColumns()-1) {
					for (int xi = x-1; xi<x+1;xi++) {
						for (int yi = y-1; yi<y+1;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
			}
		}
		return statecount;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
