package cellular;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{
    IGrid currentGeneration;
    Integer rows;
    Integer columns;
    
    public BriansBrain(int rows, int columns) {
		this.rows = rows; 
		this.columns = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return this.rows;
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return this.columns;
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int r = 0; r<rows; r++) {
			for (int c = 0; c<columns; c++) {
				CellState newState = getNextCell(r, c);
				nextGeneration.set(r, c, newState);
			}
		}
		currentGeneration = nextGeneration.copy();
	}

    @Override
	public CellState getNextCell(int row, int col) {
		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState nextState = CellState.DEAD;
		CellState currentState = getCellState(row, col);
		if (aliveNeighbors == 2 && currentState == CellState.DEAD) {
			nextState = CellState.ALIVE;
		}
		else if (currentState == CellState.ALIVE) {
			nextState = CellState.DYING;
		}
		else if (currentState == CellState.DYING) {
			nextState = CellState.DEAD;
		}
		return nextState;
    }

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		Integer x = row;
		Integer y = col;
		Integer statecount = 0; 
		if (x > 0 && x < numberOfRows() - 1 && y > 0 && y < numberOfColumns() - 1) {
			for (int xi = x-1; xi<x+2;xi++) {
				for (int yi = y-1; yi<y+2;yi++) {
					if (xi == x && yi == y) {
						continue;
					}
					else if (state == currentGeneration.get(xi, yi)) {
						statecount++;
					}
				}
			}
		}
		else {
			if (x == 0) {
				if (y == 0) {
					for (int xi = x; xi<x+2;xi++) {
						for (int yi = y; yi<y+2;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
				else if (y == numberOfColumns()-1) {
					for (int xi = x; xi<x+2;xi++) {
						for (int yi = y-1; yi<y+1;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
			}
			else { 
				if (y == 0) {
					for (int xi = x-1; xi<x+1;xi++) {
						for (int yi = y; yi<y+2;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
				else if (y == numberOfColumns()-1) {
					for (int xi = x-1; xi<x+1;xi++) {
						for (int yi = y-1; yi<y+1;yi++) { 
							if (xi == x && yi ==y){
								continue;
							}
							else if (state == currentGeneration.get(xi, yi)) {
								statecount++;
							}
						}
					}
				}
			}
		}
		return statecount;
	}

    @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
